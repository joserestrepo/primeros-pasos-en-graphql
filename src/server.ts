/**
 * Importando dependencias necesarias
 */
import express from "express";
import compression from 'compression';
import cors from 'cors';

import schema from './schema/index';
import { ApolloServer } from 'apollo-server-express';
import { createServer } from "http";

const app = express();

app.use('*',cors());

app.use(compression());

const apolloServer = new ApolloServer({
    schema,
    introspection: true
})

apolloServer.applyMiddleware({ app });

const httpServer = createServer(app);

const PORT = 3000;

httpServer.listen(
    {port: PORT},
    ()=>console.log(`API corriendo en http://localhost:${PORT}/graphql`)
)